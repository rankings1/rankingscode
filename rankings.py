COMMA = ','
PASS = 50
def parse_line(line: str)-> list:
    name, *marks = line.strip().split(' ')
    marks = [int(_) for _ in marks]
    return [name] + marks 

def load_data(mark_file: str) -> list:
    return [parse_line(line) for line in open(mark_file)]

def arrange(mark_list: list)->list:
    list_ = []
    new_list = []
    def make_key(one: list)->str:
        for i in mark_list:
              list_.append((i[1],i[2],i[3],i[0]))
        return sorted(list_,reverse=True)
    
    
    return make_key(mark_list)

def assign_rank(score_wise: list)-> list:
    score = -1
    rank = 0
    assigned = []
    for position, one_score in enumerate(score_wise,start=1):
        if one_score[-1] != score:
            rank = position
            score = one_score[-1]
        
        assigned.append(f'{rank:4} {one_score[3]:5}{one_score[0]:40}{one_score[1]:5}{one_score[2]:5}')
    return assigned

print(load_data('studentRanking.txt'))
print(arrange(load_data('studentRanking.txt')))
for _ in assign_rank(arrange(load_data('studentRanking.txt'))):
    print(_)

